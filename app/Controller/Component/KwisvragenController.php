<?php
session_start();
App::uses('AppController', 'Controller');
//App::uses('CSS','CSS');
#use App\Controller\AppController;
#use App\Model\Kwisvraag;
#use Cake\I18n\Time;
#use Cake\ORM\TableRegistry;


class KwisvragenController extends AppController {

			public $helpers = array('Html', 'Form');

			/**
			* Initializes session variables and number values for resetting scores at instantiation of this class
			*/
      public function ___construct(){

        $_SESSION['wrongcount']=0;
        $_SESSION['correctcount']=0;
				$first_number=0;
				$second_number=0;
				$answer=0;
				$correctanswer=0;
				$totalanswered=0;
      }


			/**
			* The function index randomly assigns a multiplication, division, substarction or addition question
			*/
			public function index() {

				//if (! isset( $_SESSION['correctcount'])); echo 'Wel waarde';$_SESSION['correctcount']=0;
				//if (! isset( $_SESSION['wrongcount'])); echo 'Wel waarde'; $_SESSION['wrongcount']=0;

				// Array gevuld met toegestane bewerkingen op getallen
				$operators=array('addition','substraction','multiplication','division');
				// Generereer het eerste deel van de opgave
				$first_number=rand(0,100);
				// Generereer het tweede deel van de opgave
				$second_number=rand(0,100);
				// Zorg ervoor dat er zo nu en dan andere bewerkingen gekozen worden
				shuffle($operators);

				// Er voor zorgen, dat de waardes voor het eerste en het tweede deel van de opgave, binnen de eerste honderd getallen toegekend worden
        if ( $first_number < 101 && $first_number > -1 && $second_number < 101 && $second_number > -1){

						// Logica bij minsommen: het eerste getal moet groter zijn dan het tweede getal. Lukt het niet, dan wordt een optelsom aangeboden.
						if ( $operators[0] == 'substraction'  && ( $first_number > $second_number )){
							// Opgave kan gerenderd worden
							$som = $first_number.'-'.$second_number;
							//Bepaal het antwoord, zodat het meegezonden kan worden naar het formulier in index.ctp
							$answ = $first_number-$second_number;
						}else{
							$som = $first_number.'+'.$second_number;
							$answ = $first_number+$second_number;
						}//end if


					// Logica bij optelsommen: geen speciale verificatie nodig
					if ( $operators[0] == 'addition' ){
						// Opgave kan gerenderd worden
						$som = $first_number.'+'.$second_number;
						//Bepaal het antwoord, zodat het meegezonden kan worden naar het formulier in index.ctp
						$answ = $first_number+$second_number;
					}//end if


					// Logica bij deelsommen: Restant na modulo van de opgave moet 0 zijn en tweede deel mag geen nul zijn. Lukt het niet, dan wordt een optelsom aangeboden.
					if ( $operators[0] == 'division' && ($second_number !== 0) && ( $first_number % $second_number == 0)){
						// Opgave kan gerenderd worden
						$som = $first_number.':'.$second_number;
						//Bepaal het antwoord, zodat het meegezonden kan worden naar het formulier in index.ctp
						$answ = $first_number/$second_number;
					}else{
						$som = $first_number.'+'.$second_number;
						$answ = $first_number+$second_number;
					}//end if

					// Logica voor het aanbieden van een vermenigvuldiging. Ervoor zorgen dat een van de beide somdelen maximaal de waarde 10 bevat
					if ( $operators[0] == 'multiplication'  && ( $first_number < 11 || $second_number < 11 )){
						// Opgave kan gerenderd worden
						$som = $first_number.'x'.$second_number;
						//Bepaal het antwoord, zodat het meegezonden kan worden naar het formulier in index.ctp
						$answ = $first_number*$second_number;
					}//end if
				}//end if

				 //Doorgeven aan de view in index.ctp
				 $this->set(compact('som'));
				 $this->set(compact('answ'));


				 // Form post data verwerken
				 if($this->request->is('post')){

					 	  // Wanneer gebruiker 'Stop' button heeft gedrukt, resultaten verwerken
 			        if ( isset( $this->request->data['stopMe'] )){

 			            $this->results();

 			        }

							if ( !isset( $this->request->data['stopMe'] )){
										// Opvragen gegevens uit het postMe veld (Dit is het door de gebruiker ingevoerde antwoord)
							  	  $answer = $this->request->data['postMe'];
										// Opvragen gegevens uit het answ veld (Dit is het juiste antwoord)
							  		$correctanswer = $this->request->data['answ'];

										// Wanneer het gegeven antwoord niet leeg is EN het gegeven antwoord overeenkomt met de antwoord waarde vanuit het systeem.
										// Met === wordt ook gegekeken of de datatypes gelijk zijn.
										//if ( $answer === $correctanswer && ! empty($answer) ){
										if ( $answer === $correctanswer) {

												// Update counter voor correcte antwoorden
					                $this->correctcount(1);

										}
										// Wanneer het gegeven antwoord niet leeg is EN het gegeven antwoord niet overeenkomt met de antwoord waarde vanuit het systeem
										// Voorkomen dat een druk op de stop knop als een fout antwoord geregistreerd wordt
										//if ( $answer !== $correctanswer && !empty($answer) && !isset($_POST['stopMe']) ){
										if ( $answer !== $correctanswer && !isset($_POST['stopMe']) ){
												 $this->wrongcount(1);

				        		}
							 }
										// Totaal aantal gemaakte opgaves berekenen en klaar maken voor gebruik in index.ctp en opslag in tabel
 			        $totalanswered= $_SESSION['correctcount']+$_SESSION['wrongcount'];
 			        $this->set(compact('totalanswered'));


 			     }
			}//end function


			// Actual increase of `correct` counter
			public function correctcount($addcnt){

				  if(isset($_SESSION['correctcount']))$_SESSION['correctcount'] ++;
			    $this->Flash->set('Goed');
			}

			// Actual increase of `wrong` counter
			public function wrongcount($addcnt){
				if (isset( $_SESSION['wrongcount'] ))$_SESSION['wrongcount']++;
			    $this->Flash->set('Jammer');

			}

			// Result presenteert een overzicht van de resultaten na een sessie. Wordt in werking gesteld, nadat gebruiker op Stop knop heeft gedrukt
			public function results(){

			        $totalanswered= $_SESSION['correctcount']+$_SESSION['wrongcount'];
			        $goed=$_SESSION['correctcount'];
			        $fout=$_SESSION['wrongcount'];

			        //Prevent user from early stopping exercizes. User has to play 10 exercises
			        if ($totalanswered > 0 ){

			              $score=$goed/$totalanswered;
			              echo('<div id="container">
			              Je resultaat is:<br>
			              Aantal antwoorden: '.$totalanswered.'<br>
			              Aantal goed :'.$goed.'<br>
			              Aantal fout:'.$fout.'<br>
			              Score:'.number_format((float)$score, 2,'.',' ').'
			              </div>'
			                );
			                $_SESSION['correctcount']=0;
			                $_SESSION['wrongcount']=0;
											$this->save2table( $score );
			      }else{

			              $this->Flash->set('Je moet minimaal 10 sommen maken');
			      }

			}

			// Laadt de highscores uit tabel en presenteert ze, wanneer de link hiscores wordt ingedrukt
			public function getscores(){

			      $hiscores = TableRegistry::getTableLocator()->get('kwisvragens');
			      $query = $this->Kwisvragen
			              ->find()
			              ->select(['hs1'])
			              ->order(['hs1' => 'DESC'])
			              ->limit(5);
			      $query = $this->set(['query' => $query]);

			}

			// Bewaar de score van de gebruiker in een tabel
			 public function save2table( $score ){
				 	//Timestamp voor e_id veld genereren
			     $date = strtotime('now');
					 // Data opmaken
					 $hstable = array('e_id'=>h($date),'hs1'=>number_format((float)$score, 2,'.',' '));
					 // Opslaan
			     if( $score >= 0 &&  $this->Kwisvragen->save($hstable) ){

			          $this->Flash->set('Score is opgeslagen');

			     }

			 }



}//end class
